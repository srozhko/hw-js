// Завдання
// Реалізувати можливість зміни колірної теми користувача.Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки
// Література:
// LocalStorage на пальцях

window.onload = function () {
    if (localStorage.getItem('bgcolor') !== null) {
        let color = localStorage.getItem('bgcolor')
        document.body.style.backgroundColor = color
    } 
    

let changeStyle = document.body
let chageColor = document.querySelectorAll('li')
const btn = document.querySelector('.switch-btn')
btn.addEventListener('click', ()=>{
    if (!btn.classList.contains('switch-on')) {
        btn.classList.add('switch-on')
        changeStyle.style.backgroundColor = "black"
        localStorage.setItem('bgcolor', 'black')
    } else {
        btn.classList.remove('switch-on')
        changeStyle.style.backgroundColor = ""
        localStorage.setItem('bgcolor', '')

    }
//    btn.classList.toggle('switch-on')
//    changeStyle.style.backgroundColor = "black"
//    localStorage.setItem('bgcolor', 'black')
//    chageColor.forEach((item)=>{
//         item.style.color ="red"
//    })
   
})

}