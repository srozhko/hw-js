// Теоретичні питання
// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Завдання
// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
// Література:
// Клавіатура: keyup, keydown, keypress

// let button = document.querySelector('.btn-wrapper')
	const coloredBtnFn = () =>{
		let btn = document.querySelectorAll('.btn')
	// 	btn.forEach((item) => {
	// 		item.style.backgroundColor = "red"
	// 		// if (item.classList.contains('active')){
	// 		// 	item.classList.remove('active')
	// 		// }

	// 		if (event.code === 'Enter' && item.id === 'Enter') {
	// 			item.style.backgroundColor = "blue"
	// 			// console.log(event.code);
	// 		} else if ( event.code === 'KeyS' && item.id === 'S') {
	// 			item.style.backgroundColor = "blue"
	// 		} else if ( event.code === 'KeyE' && item.id === 'E') {
	// 			item.style.backgroundColor = "blue"
	// 		} else if ( event.code === 'KeyO' && item.id === 'O') {
	// 			item.style.backgroundColor = "blue"
	// 		} else if ( event.code === 'KeyN' && item.id === 'N') {
	// 			item.style.backgroundColor = "blue"
	// 		} else if ( event.code === 'KeyL' && item.id === 'L') {
	// 			item.style.backgroundColor = "blue"
	// 		} else if ( event.code === 'KeyZ' && item.id === 'Z') {
	// 			item.style.backgroundColor = "blue"
	// 		} 
	// })
	document.addEventListener('keypress', (event)=>{
	for (let elem of btn) {
		elem.style = 'background:black'
		if (elem.innerText.toUpperCase() == event.key.toUpperCase()) {
			elem.style = 'background:blue'
		}
	}	
	console.log(event.key);
	})

}
 coloredBtnFn()

// const clickFn =(event) =>{
// 	let btn = document.querySelectorAll('.btn')
// 	btn.forEach((item)=>{
// 		if (item.classList.contains('active')){
// 			item.classList.remove('active')
// 		}	
// 	})
// 	if (!event.target.classList.contains('active')) {
// 		event.target.classList.add('active')
// 	}
// }
// let button = document.querySelector('.btn-wrapper')
//     button.addEventListener('click', (event)=>{
//         if (event.target.nodeName ==="BUTTON") {
//             clickFn(event)
//         };
//     })
