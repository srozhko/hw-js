// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
// Література:
// Використання data-* атрибутів

 
// const tabTitle= document.querySelectorAll('.tabs-title')
// const tabContent = document.querySelectorAll('.tabs-content li')

// tabTitle.forEach(onTabClick);

//  function onTabClick(item, index) {
//     item.setAttribute('data-tab', `#tab-${index+1}`)
//     item.addEventListener("click", ()=> {
//         let currentBtn = item
//         let tabId = currentBtn.getAttribute("data-tab")
//         let currentTab = document.querySelector(tabId)
//         if (!currentBtn.classList.contains('active')) {
//             tabTitle.forEach((item)=>{
//                 item.classList.remove('active')
//             });
//             tabContent.forEach((item)=>{
//                 item.classList.remove('active')
//             });

//             item.classList.add('active')
//             currentTab.classList.add('active')  
//         }  
//     })
//  }
// console.log(tabTitle);


// tabContent.forEach((element, index) => {
//     element.setAttribute('id', `tab-${index+1}`)
// });
// console.log(tabContent);


const  tabsFn= ()=>{

    const changeFn= (event)=>{
        let menuItems= document.querySelectorAll('[data-tab]')
        menuItems.forEach(item=>{
            if (item.classList.contains('active')){
                item.classList.remove('active')
            }
        })

        if (!event.target.classList.contains('active')) {
            event.target.classList.add('active')
        }
        let id = event.target.getAttribute('data-tab')
        let conteiners = document.querySelectorAll('.tab-item')
        conteiners.forEach(item=>{
            if (item.classList.contains('active')){
                item.classList.remove('active')
            }
        })
        if (!document.getElementById(id).classList.contains("active")) {
            document.getElementById(id).classList.add("active")
        }
    }

    let tabs = document.querySelector('.tabs')
    tabs.addEventListener('click', (event)=>{
        if (event.target.nodeName ==="LI") {
            changeFn(event)
        };
    })
}
tabsFn()