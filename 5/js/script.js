// Теоретичні питання
// Опишіть своїми словами, що таке метод об'єкту? Передача большого количества информации + возможность написания меньше кода
// Який тип даних може мати значення властивості об'єкта? Властивість може мати будь-який тип даних. 
// Об'єкт це посилальний тип даних. Що означає це поняття? Тобто ми можемо використовувати об'єкт замість змінної.
// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
// Необов'язкове завдання підвищеної складності
// Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
// Література:
// Об'єкти як асоціативні масиви
// Object.defineProperty()



const createNewUser=() => {
    let firstName = prompt ("Введіть ім'я:")
    let lastName = prompt ("Введіть призвіще:")
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin() { 
            return (this.firstName[0] + this.lastName).toLowerCase()
        }
        // setFirstName(value){
        //     Object.defineProperty(this, firstName, {
        //         value:value,
        // })
        //         // firstName = value;
        // },
        // setLastName(value){
        //     Object.defineProperty(newUser, lastName, {
        //         value:value,
        //     })
        //     // lastName = value;
        // },
    }
    return newUser
}
const user = createNewUser ()
console.log(user);
console.log(user.getLogin());

// newUser.setFirstName("Maks");
// newUser.setLastName("Barskyh");
// console.log(user);
