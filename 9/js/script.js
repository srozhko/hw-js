// Теоретичні питання
// Опишіть, як можна створити новий HTML тег на сторінці. За допомогою document.createElement()
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра. Перший параментр вказує куди ми виводимо нашу інформацію. Є 4 розміщення: "afterend" в коді HTML посля елемента(тега), "beforeend"- в кінці елемента, "beforebegin"- перед елементом, "afterbegin"- в самому початку в елемнті
// Як можна видалити елемент зі сторінки? За допомогою метода .remove()

// Завдання
// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.
// Необов'язкове завдання підвищеної складності:
// Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список. Приклад такого масиву:

// ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
// Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.
// Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

// Література:
// Пошук DOM елементів
// Додавання та видалення вузлів
// Шаблонні рядки
// Array.prototype.map()
// setTimeout, setInterval

const list = document.createElement( 'div' );
list.classList.add( 'list-item' );
document.body.prepend(list);
let newUl = document.createElement("ul")
list.prepend(newUl)

const arrayList = (array, arg) => {
    array.forEach((element, index) => {
        // let newLi = document.createElement('li')
        // newLi.classList.add(`item-${index+1}`)
        // newUl.prepend(element)
        arg.insertAdjacentHTML("afterbegin", `<li class=item-${index+1} >${element}</li>`)
        // newUl.style.color= "red"
        // setTimeout(()=> list.remove(), 3000)
    });
}
arrayList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], newUl)
// arrayList(["1", "2", "3", "sea", "user", 23])