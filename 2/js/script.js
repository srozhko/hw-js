// Теоретичні питання
// Які існують типи даних у Javascript? 
// Існує 8 видів: числовий, строковий, булевий, null, щб'єкт, undefined, typeof, biglnt
// У чому різниця між == і ===?
// При нестрогій нерівності == ми перевіряємо вміст + різні типи можуть показувати true, наприклад "1" = 1, а при використані суворої рівності === ми вже перевіряємо і типи даних.
// Що таке оператор?
// Cимволи які задають умови для виконання задачі  


// Завдання
// Реалізувати просту програму на Javascript, яка взаємодіятиме з користувачем за допомогою модальних вікон браузера - alert, prompt, confirm. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
// Якщо вік менше 18 років - показати на екрані повідомлення: You are not allowed to visit this website.19
// Якщо вік від 18 до 22 років (включно) – показати вікно з наступним повідомленням: Are you sure you want to continue? і кнопками Ok, Cancel. Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача. Якщо користувач натиснув Cancel, показати на екрані повідомлення: You are not allowed to visit this website.
// Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
// Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
// Після введення даних додати перевірку їхньої коректності. Якщо користувач не ввів ім'я, або при введенні віку вказав не число - запитати ім'я та вік наново (при цьому дефолтним значенням для кожної зі змінних має бути введена раніше інформація).

let usName;
do{
   usName = prompt ("Введіть своє ім'я");
}while (!Boolean(usName) || !isNaN(Number(usName)))
console.log(usName);
let age;
do{
   age = prompt ("Скільки вам років?");
}while (!Boolean(age) || isNaN(age) || age ===" ") 
console.log(age);
   if (age < 18) {
      alert ("You are not allowed to visit this website.")
   } else if (age>= 18 && age<=22) {
      let answer = confirm ("Are you sure you want to continue?")
      if (answer === true) {
         alert ("Welcome, " + usName)
      } else {
         alert ("You are not allowed to visit this website") }
   } else {
      alert ("Welcome, " + usName)
   }