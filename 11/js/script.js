// Завдання
// Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.


// 1. находим форму и наши елементы которые в ней есть
// 2. даем событие для наших импутов(нажатие на кнопки). в функции видим какие клавиши юзер нажимает. 
// 3. Все буквы собираем в один масив
// 4. делаем функцию которая будет при клике отображать наш масив + меняем цвет иконце при ее нажимании
// 5. делаем проверку 2-х импутов при нажатии на кнопку. и ифами делаем отображение если сходить, не сходиться. 

const inputFirst = document.querySelector('.input-password-first')
const inputSecond = document.querySelector('.input-password-second')

const showPassword = () => {
   const btnShowfirst = document.querySelector('.icon-password-first') 
   btnShowfirst.addEventListener('click', ()=>{
      btnShowfirst.classList.toggle('fa-eye-slash')
      btnShowfirst.style.display= "block"

      if (inputFirst.getAttribute('type')=== 'password') {
         inputFirst.setAttribute('type', 'text')
      } else {
         inputFirst.setAttribute('type', 'password')
      }
   })
   const btnShowSecond = document.querySelector('.icon-password-second') 
   btnShowSecond.addEventListener('click', ()=>{
      btnShowSecond.classList.toggle('fa-eye-slash')
      btnShowSecond.style.display= "block"

      if (inputSecond.getAttribute('type')=== 'password') {
         inputSecond.setAttribute('type', 'text')
      } else {
         inputSecond.setAttribute('type', 'password')
      }
   })
}
showPassword()

const btn = document.querySelector('.btn')
btn.addEventListener("click", (event)=>{
   if (inputFirst.value !== inputSecond.value) {
		let redError = document.createElement( "p" );
		redError.innerHTML = "<div>Потрібно ввести однакові значення</div>";
		redError.style.color = 'red';
      // inputSecond.insertAdjacentHTML('beforeend', '<strong>Потрібно ввести однакові значення</strong')
		btn.before(redError)
   } else {
      alert('You are welcome')
   }
   btn.preventDefault()
})

