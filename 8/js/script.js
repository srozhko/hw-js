// Теоретичні питання
// Опишіть своїми словами що таке Document Object Model (DOM) 
// Это объекты. Основным файлом для DOM cчитаеться HTML документ. Из которого мы можем получить информацию, а также ее изменить или создать в JS. 
// Яка різниця між властивостями HTML-елементів innerHTML та innerText? З допомогою innerHTML ми можемо вставити код з тегом і вложеними елементами і текстом. А при innerText можемо вставити тільки текст. Тобто якщо ми візьмемо код, то він буде вставлятись як текст, а не як код.  
// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий? Можна звертатись за допомою тега, класу, атрибута. На данний момент уніврсальний спосіб звернення до елемента є document.querySelectorAll(). Так як він є універсальним. Але в той же час він виводить статичну колекцію, а не живу. Що може бути інколи проблемою. Тобто в статичній колекції не відображаться дані після її зміни. 
// Завдання
// Код для завдань лежить в папці project.

// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

// Отримати елементи

// , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

// Література:
// Пошук DOM елементів

const getParagraphs = document.getElementsByTagName('p');
for (const elem of getParagraphs) {
    elem.style.backgroundColor = "#ff0000";
}
console.log(getParagraphs);

const getOptionList = document.querySelector('#optionsList');
console.log(getOptionList);
console.log(document.querySelector('#optionsList').parentNode);
console.log(document.querySelector('#optionsList').hasChildNodes());
let childList = document.querySelector('#optionsList').children
console.log(childList)
for (let node of childList) {
    console.log(node);
    // console.log(typeof node)
}

let newParagraph = document.querySelector('#testParagraph')
newParagraph.innerHTML = "This is a paragraph"
// console.log(newParagraph);

let getNewItemMenu= document.querySelectorAll(".main-header")
for (const elem of getNewItemMenu) {
    let newClassName = elem;
    newClassName.classList.add('nav-item')
    console.log(elem);
}

 const getSectionTitle = document.querySelectorAll('.products-title')
 getSectionTitle.forEach(element => {
    //  element.classList.remove('products-title')
     element.classList.replace('products-title', 'section-title')
    // так как в html нет класса с нужным нам значением, то сделал 2 метода. один удаляет, другой заменяет название классаю 
 });
//  console.log(getSectionTitle);
