// Теоретичні питання
// Як можна оголосити змінну у Javascript? 
// За допомогою: var, let, const (може мати тільки одне значення)
// У чому різниця між функцією prompt та функцією confirm? 
// В функції prompt необхідно/можливо вписати відповідь в поле вводу. Тоді як confirm дає можливість відповісти тільки 2-ма кнопками при натисканні однієї на вибір.    
// Що таке неявне перетворення типів? Наведіть один приклад.
// це коли використовується 2 різних типа, наприкад числовий з строковим. 456 + "345"



// Завдання
// Оголосіть дві змінні: admin та name. Встановіть ваше ім'я в якості значення змінної name. Скопіюйте це значення в змінну admin і виведіть його в консоль.
let admin;
let name = "Stas";
admin = "Stas";
console.log(admin);
console.log(typeof admin);
// Оголосити змінну days і ініціалізувати її числом від 1 до 10. Перетворіть це число на кількість секунд і виведіть на консоль.
let days = 6;
// days = days * 86.4e3
days *= 86.4e3 
console.log(days + " sec");
console.log(typeof days);
// Запитайте у користувача якесь значення і виведіть його в консоль.
const answer = +prompt("Скільки чашок кави ви випиваєте за день?");
console.log(answer, typeof answer);


