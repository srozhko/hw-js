// Теоретичні питання
// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Це спеціальні спецсимволи яку задаються за допомогою "\". Ми можемо добавити в текст дужки, лапки, смайлики і т.д.
// Які засоби оголошення функцій ви знаєте? Є два види оголошення функції declaration, expression
// Що таке hoisting, як він працює для змінних та функцій? В переводі "піднімання", тобто це механізм який спрацьовує в JS. Наприклад для функції declaration ми можемо її визвати перед ссамою функцією (написаним кодом), а для функції expression ми вже цього зробити не зможемо. Ми можемо визивати функцію після її оголошення.
// Завдання
// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
// Література:
// Дата і час


const createNewUser=() => {
    let firstName = prompt ("Введіть ім'я:")
    let lastName = prompt ("Введіть призвіще:")
    let birthday = prompt(`Введіть дату народження: дд.мм.гггг`)
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin() { 
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        birthday: birthday,
        getAge() {
            // console.log(new Date(this.birthday.substring(6)).getFullYear());
            return new Date().getFullYear()-new Date(this.birthday.substring(6)).getFullYear()
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase()+this.birthday.substring(6)
        }
    }
    return newUser
}
const user = createNewUser ()
console.log(user);
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
